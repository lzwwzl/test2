package com.demo01.server;

import org.tio.server.ServerTioConfig;
import org.tio.server.TioServer;
import org.tio.server.intf.ServerAioHandler;
import org.tio.server.intf.ServerAioListener;

import java.io.IOException;

public class Server {
    public static void main(String[] args) throws IOException {
        //t-io服务端处理器。包含编码、解码、消息处理
        ServerAioHandler handler = new ServerDemoHandler();
        //t-io服务端链路监听器。包含各种回调钩子方法
        ServerAioListener listener = new ServiceDemoListener();
        //服务端配置类
        ServerTioConfig serverTioConfig = new ServerTioConfig("ServerDemo", handler, listener);
        //服务端启动类
        TioServer tioServer = new TioServer(serverTioConfig);
        tioServer.start(null, 10010);
    }
}
