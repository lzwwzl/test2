package com.demo01.core;

import org.tio.core.intf.Packet;

/**
 * 自定义的一个数据业务包
 */
public class DemoPacket extends Packet {

    private static final long serialVersionUID = -1L;

    /**
     * 为什么定义长度是4？
     * <p>
     * 原因就是java中，一个int占4个字节数
     */
    public static final int HEAD_LENGTH = 4;

    private byte[] body;

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

}
