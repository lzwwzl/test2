package com.demo01.client;

import com.demo01.core.DemoPacket;
import org.tio.client.ClientChannelContext;
import org.tio.client.ClientTioConfig;
import org.tio.client.ReconnConf;
import org.tio.client.TioClient;
import org.tio.client.intf.ClientAioHandler;
import org.tio.client.intf.ClientAioListener;
import org.tio.core.Node;
import org.tio.core.Tio;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Client {
    public static void main(String[] args) throws Exception {
        Node serverNode = new Node("127.0.0.1", 10010);
        ReconnConf reconnConf = new ReconnConf(5000L);
        ClientAioHandler clientHandler = new ClientDemoHandler();
        ClientAioListener clientListener = new ClientDemoListener();
        ClientTioConfig clientTioConfig = new ClientTioConfig(clientHandler, clientListener, reconnConf);
        clientTioConfig.setHeartbeatTimeout(5000);
        TioClient tioClient = new TioClient(clientTioConfig);
        ClientChannelContext clientChannelContext = tioClient.connect(serverNode);

        TimeUnit.SECONDS.sleep(1);
        DemoPacket demoPacket = new DemoPacket();
        demoPacket.setBody(String.format("现在是北京时间%s", new Date()).getBytes(StandardCharsets.UTF_8));
        Tio.send(clientChannelContext, demoPacket);
    }
}
